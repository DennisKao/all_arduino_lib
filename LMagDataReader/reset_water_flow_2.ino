#include "ReadLMag.h"
#define MODBUS_PIN 9

ReadLMag reader = ReadLMag(MODBUS_PIN);

void setup() {
    Serial.begin(9600);
    unsigned long now = millis();    
    reader.init(now);
    Serial.println("init");
}

unsigned long st = 0;

void loop() {
    unsigned long now = millis();
    reader.poll(now);

    // debug
    if (now -st >= 1000){
        st = now;
        reader.print_data();
    }
    

}
