//
// Created by elijah on 2019/3/4.
//

#ifndef LIBRARIES_READMODBUS_H
#define LIBRARIES_READMODBUS_H
#endif //LIBRARIES_READMODBUS_H
#include "Arduino.h"

#ifndef MODBUS_SERIAL
#define MODBUS_SERIAL Serial2
#endif

/**
 * 網路上抓的  列印浮點數的函數
 * @param val   浮點數
 * @param precision     要顯示的位數
 */
void printDouble( double val, byte precision){
    // prints val with number of decimal places determine by precision
    // precision is a number from 0 to 6 indicating the desired decimial places
    // example: lcdPrintDouble( 3.1415, 2); // prints 3.14 (two decimal places)

    if(val < 0.0){
        Serial.print('-');
        val = -val;
    }

    Serial.print (int(val));  //prints the int part
    if( precision > 0) {
        Serial.print("."); // print the decimal point
        unsigned long frac;
        unsigned long mult = 1;
        byte padding = precision -1;
        while(precision--)
            mult *=10;

        if(val >= 0)
            frac = (val - int(val)) * mult;
        else
            frac = (int(val)- val ) * mult;
        unsigned long frac1 = frac;
        while( frac1 /= 10 )
            padding--;
        while(  padding--)
            Serial.print("0");
        Serial.print(frac,DEC) ;
    }
}

/**
 * @struct 紀錄每台流量計的資料集合
 */
struct LMagData{
    float instant_flow_value;   // 瞬時流量
    float instant_flow_rate;    // 瞬時流速
    unsigned long total_positive; // 正向累積
};

/**
 * 輪詢的進度狀態 : 發送命令   正在等待回復   接收回復訊息
 */
enum ReadLMagMode{SEND, WAIT, RECEIVE};

class ReadLMag{
public:
    ReadLMag(int pin);
    void test();    // 測試用
    bool poll(unsigned long now);   // 輪詢
    void reset();   // 流量計歸零
    LMagData data_list[4];  // 存放解析完畢的資料
    double total[4] = {0};      // 存放總流量
    void print_data();
    void init(unsigned long now);
private:
    HardwareSerial *port = &MODBUS_SERIAL;
    int pin;    // rs-485 to ttl 通訊pin
    int current_slave_id = 1;   // 現在詢問的設備id
    const int slave_id_max = 4;
    void send_request(int slave_id);    // 發送詢問
    uint16_t return_buffer[50];     // 回傳值緩存區
    const uint16_t request_data[4][8] = {
            {0x01, 0x04, 0x10, 0x10, 0x00, 0x16, 0x74, 0xC1, },
            {0x02, 0x04, 0x10, 0x10, 0x00, 0x16, 0x74, 0xF2, },
            {0x03, 0x04, 0x10, 0x10, 0x00, 0x16, 0x75, 0x23, },
            {0x04, 0x04, 0x10, 0x10, 0x00, 0x16, 0x74, 0x94, },
    };
    unsigned long interval = 10;
    unsigned long time_stamp = 0;
    unsigned long timeout = 1000;
    unsigned long timeout_time_stamp = 0;
    unsigned long add_last_time[4] = {0};       // 上次累加流量時間
    ReadLMagMode mode = SEND;
    void decode_data();
    void add_total(unsigned long now);
    float fill_up_parameter(int index);
    float get_float(uint16_t part_a, uint16_t part_b, uint16_t part_c, uint16_t part_d);
    int d = 65; // 管道直徑 單位:mm

};
/**
 * @param pin  通訊用pin
 */
ReadLMag::ReadLMag(int pin) {
    this->pin = pin;
    pinMode(pin, OUTPUT);
    port->begin(9600);
}

void ReadLMag::test() {
    for(int i = 0; i<4; i++){
        Serial.print(total[i]/1000);
        Serial.print("L  ");
    }
    Serial.println();
}

void ReadLMag::send_request(int slave_id) {
    uint16_t *data = this->request_data[slave_id-1];
    digitalWrite(this->pin, HIGH);
    for(int i=0; i<8; i++){
        port->write(*(data+i));
    }
    port->flush();
    digitalWrite(this->pin, LOW);
}

bool ReadLMag::poll(unsigned long now) {
    if (now - time_stamp < interval){ return false;}
    time_stamp = now;

    // 檢查設備id overflow
    if (current_slave_id > slave_id_max){
        current_slave_id = 1;
    }

    // test
    //current_slave_id = 2;

    if (mode == SEND){  // 發送模式
        timeout_time_stamp = now;
        send_request(current_slave_id);
        mode = WAIT;
    } else if (mode == WAIT){     // 等待接收模式
        if(now - timeout_time_stamp > timeout){     // 逾時
            mode = SEND;
            add_last_time[current_slave_id-1] = now;
            data_list[current_slave_id - 1].instant_flow_rate = 0;  // 沒聯絡到就當作0
            data_list[current_slave_id - 1].instant_flow_value = 0;
            current_slave_id += 1;
        } else if (port->available()){     // 開始有訊號傳入
            mode = RECEIVE;
        }
    } else{
        int index = 0;
        while (port->available()){
            return_buffer[index] = port->read();
            index += 1;
            delay(1);
        }
        mode = SEND;
        decode_data();
        add_total(now);
        current_slave_id += 1;
        return true;
    }
    return false;
}

float ReadLMag::fill_up_parameter(int index){
    return get_float(
            return_buffer[index],
            return_buffer[index+1],
            return_buffer[index+2],
            return_buffer[index+3]
    );
}

void ReadLMag::decode_data() {
    const int i = return_buffer[0] - 1;
    if(i < 0 || i > slave_id_max - 1){
        Serial.println("index out of range");
        return;
    }
    data_list[i].instant_flow_value = fill_up_parameter(3);
    data_list[i].instant_flow_rate = fill_up_parameter(7);

    // 正向整數累積
    unsigned long tmp = 0; // 8bit to 32bit number
    tmp = return_buffer[19];
    tmp = tmp << 8;
    tmp += return_buffer[20];
    tmp = tmp << 8;
    tmp += return_buffer[21];
    tmp = tmp << 8;
    tmp += return_buffer[22];
    data_list[i].total_positive = tmp;
}

float ReadLMag::get_float(uint16_t part_a, uint16_t part_b, uint16_t part_c, uint16_t part_d) {
    union{
        unsigned long l;
        float f;
    }tmp;
    tmp.l = part_a;
    tmp.l = tmp.l<<8;
    tmp.l += part_b;
    tmp.l = tmp.l<<8;
    tmp.l += part_c;
    tmp.l = tmp.l<<8;
    tmp.l += part_d;
    return tmp.f;
}

void ReadLMag::reset(){
    Serial.println("reset to zero!!!");
    const uint16_t data_lis[4][8] = {
            {0x01, 0x06, 0x00, 0x47, 0xA5, 0x5A, 0xC2, 0xB4, },
            {0x02, 0x06, 0x00, 0x47, 0xA5, 0x5A, 0xC2, 0x87, },
            {0x03, 0x06, 0x00, 0x47, 0xA5, 0x5A, 0xC3, 0x56, },
            {0x04, 0x06, 0x00, 0x47, 0xA5, 0x5A, 0xC2, 0xE1, },
    };

    for (int i = 0; i < 4; i++){
        uint16_t *data = data_lis[i];

        delay(100);
        digitalWrite(this->pin, HIGH);
        for(int i=0; i<8; i++){
            port->write(*(data+i));
        }
        port->flush();
        digitalWrite(this->pin, LOW);

        delay(100);
        digitalWrite(this->pin, HIGH);
        for(int i=0; i<8; i++){
            port->write(*(data+i));
        }
        port->flush();
        digitalWrite(this->pin, LOW);
    }
}

void ReadLMag::add_total(unsigned long now) {
    const int index = return_buffer[0] - 1;
    if(index < 0 || index > slave_id_max - 1){
        Serial.println("index out of range");
        return;
    }
    double value_pre_sec = data_list[index].instant_flow_rate*d*d*M_PI_4;
    double value = value_pre_sec*(now - add_last_time[index])/1000;
    if (value > 0){
        total[index] += value;
    };
    add_last_time[index] = now;
}

void ReadLMag::print_data() {
    for(int i = 0; i<4; i++){
        Serial.print(" ID:");
        Serial.print(i+1);
        Serial.print(" 流速:");
        printDouble(data_list[i].instant_flow_rate, 5);
        Serial.print(" 總量:");
        Serial.print(data_list[i].total_positive);
    }
    Serial.println();
}

void ReadLMag::init(unsigned long now) {
    for(int i=0; i<4;i++){
        add_last_time[i] = now;
    }
}


