//
// Created by elijah on 2019/1/21.
//

#include "NexAnimate.h"

NexAnimate::NexAnimate(uint8_t pid, uint8_t cid, const char *name)
    :NexTouch(pid, cid, name)
{
}


bool NexAnimate::setEn(uint32_t number) {
    char buf[10] = {0};
    String cmd;

    utoa(number, buf, 10);
    cmd += getObjName();
    cmd += ".en=";
    cmd += buf;

    sendCommand(cmd.c_str());
    return recvRetCommandFinished();
}
