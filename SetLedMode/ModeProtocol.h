//
// Created by elijah on 2019/1/11.
//

#ifndef SETLEDMODE_MODEPROTOCOL_H
#define SETLEDMODE_MODEPROTOCOL_H

#include "Range.h"
#include "FastledBridgePath.h"


class CRGB;
class LedGroup;
/**
 * @class 規定mode的格式
 */
class ModeProtocol {
public:
    virtual bool process(CRGB *leds_ptr, Range range);
};


#endif //SETLEDMODE_MODEPROTOCOL_H
