//
// Created by elijah on 2019/1/18.
//

#ifndef LIBRARIES_REDECODEINT_H
#define LIBRARIES_REDECODEINT_H

uint32_t re_decode_int(uint16_t data_1, uint16_t data_2){
    uint32_t i;
    i = data_1;
    i = i << 16;
    i += data_2;
    return i;
}

#endif //LIBRARIES_REDECODEINT_H
