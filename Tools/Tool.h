//
// Created by elijah on 2019/1/18.
//

#ifndef LIBRARIES_TOOL_H
#define LIBRARIES_TOOL_H

//#include "ReDecodeInt.h"
#include <ModbusRtu.h>

extern Modbus master;

struct LMagData{
    float instant_flow_value;
    float instant_flow_rate;
    float flow_rate_percentage;
};

class LMagDataReader{
public:
    LMagDataReader();
    LMagData data_list[4];      // 資料
    void poll(unsigned long now);
private:
    unsigned long time_stamp = 0;
    int slave_id = 1;
    int loop_protect = 0;
    modbus_t telegram;
    uint16_t au16data[50] = {0};
    // 解析浮點數
    float get_float(uint16_t part_a, uint16_t part_b);
};

LMagDataReader::LMagDataReader() {}

void LMagDataReader::poll(unsigned long now) {
    if (now - time_stamp < 50){ return;}
    time_stamp = now;

    telegram.u8id = this->slave_id; // slave address
    telegram.u8fct = 4; // function code (this one is registers read)
    telegram.u16RegAdd = 4112; // start address in slave
    telegram.u16CoilsNo = 22; // number of elements (coils or registers) to read
    telegram.au16reg = this->au16data; // pointer to a memory array in the Arduino
    master.query(telegram); // send query (only once)

    int8_t res_len = master.poll();
    if (res_len > 0 || loop_protect > 10){       // 成功取得返回值
        // 解析
        data_list[slave_id - 1].instant_flow_value = get_float(au16data[0], au16data[1]);
        data_list[slave_id - 1].instant_flow_rate = get_float(au16data[2], au16data[3]);
        //Serial.print("成功讀取流量");
        //Serial.println(get_float(au16data[2], au16data[3]));
        slave_id += 1;
        loop_protect = 0;
    }
    loop_protect += 1;

    // 溢位檢查
    if (slave_id > 4){
        slave_id = 1;
    }
}

float LMagDataReader::get_float(uint16_t part_a, uint16_t part_b) {
    union{
        unsigned long l;
        float f;
    }tmp;
    tmp.l = part_a;
    tmp.l = tmp.l<<16;
    tmp.l += part_b;
    return tmp.f;
}






#endif //LIBRARIES_TOOL_H
